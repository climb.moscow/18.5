﻿#include <iostream>
#include <string>
class Players
{
    std::string name;
    int score;
public:
    void SetNameScore(std::string a,int b)
    {
        name = a;
        score = b;
    }
    void Print()
    {
        std::cout << name << " total score " << score << std::endl;
    }
    void GetNameScore(std::string &a, int &b)
    {
        a = name;
        b = score;
    }
};

int main()
{
    int number;
    std::cout << "Input number of players ";
    std::cin >> number;

    Players* Array = new Players[number];

    for (int i = 0; i < number; i++)
    {   
        std::string name;
        int score;
        std::cout << "Input name " << i+1 << " player ";
        std::cin >> name;
        std::cout << "Total score ";
        std::cin >> score;
        Array[i].SetNameScore(name,score);
        std::cout << std::endl;
    }
    int tmp = 0;
    while (tmp != 1)
    {
        for (int i = 0; i < number-1; i++)
        {
            tmp = 1;
            std::string name_1,name_2,name_tmp;
            int score_1,score_2,score_tmp;       
            Array[i].GetNameScore(name_1, score_1);
            Array[i+1].GetNameScore(name_2, score_2);            
            if (score_1 < score_2)
            {
                tmp = 0;
                score_tmp = score_1;
                name_tmp = name_1;
                Array[i].SetNameScore(name_2, score_2);
                Array[i + 1].SetNameScore(name_tmp, score_tmp);
            }
        }
    }
    for (int i = 0; i < number; i++) Array[i].Print();
}
